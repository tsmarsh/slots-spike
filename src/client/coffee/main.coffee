require ["lib/knockout-3.0.0", "js/machine", "lib/domReady!"], (ko, Machine) ->
    states = 
        default: 'rewarding'
        events:
            generateTiles:
                rewarding: 'generating' 
            calculateScores:
                generating: 'calculating'
            ready:
                calculating: 'ready'
            startSpin:
                ready: 'spinning'
            spinEnd:
                spinning: 'stopped'
            reward:
                stopped: 'rewarding'
                
    window.machine = new Machine(5, 64, states)
    ko.applyBindings window.machine