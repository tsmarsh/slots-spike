define ["lib/knockout-3.0.0"], (ko) ->
	class Tile
		@count = 0

		constructor: (@degree, @parent, value = 1) ->
			@id = Tile.count++
			@value = ko.observable(@padInt(value))
			@win = ko.observable(false)
			@image = ko.computed =>
				if @win() then "static/images/win_tile_" + @value() + ".jpg" else "static/images/tile_" + @value() + ".png"
			@winner = false

			if (@id % 64) < 3
				@parent.state.observeOn('spinEnd').start 'Slot Spin End' + @id, (from, to) =>
					@win(@winner)
				@parent.state.observeOn('spinStart').start 'Slot Spin Start' + @id, (from, to) =>
					@win(false)

		padInt: (i) ->
            str = "" + i
            pad = "00"
            pad.substring(0, pad.length - str.length) + str

		update: (value) =>
			@value(@padInt(value))
			@winner = Math.random() > 0.5