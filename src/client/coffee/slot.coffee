define ["lib/knockout-3.0.0", "js/tile", "js/skinny-coffee-machine", "lib/lodash"], (ko, Tile, SkinnyCoffeeMachine, jq) ->
    class Slot
        constructor: (slot_config, states) ->
            @num_tiles = slot_config['num_tiles']

            @spin = ko.observable(false)
            @duration = ko.observable(slot_config['duration'])
            @startDelay = ko.observable(slot_config['startDelay'])
            @endDelay = ko.observable(slot_config['endDelay'])
            @translate = ko.observable(1250)

            events = 
                on:
                    startSpin: (from, to) =>
                        @spin(true)
                    spinEnd: (from, to) =>
                        @spin(false)

            @state = new SkinnyCoffeeMachine jq.extend({}, states, events)
            degreesPerTile = 360 / @num_tiles
            offset = degreesPerTile * 2
            @tiles = (new Tile((degreesPerTile * i) + offset, @) for i in [0...@num_tiles])


        previous: (@previous) =>
            self = @
            @previous.state.observeOn('startSpin').start 'Slot Spinner', (from, to) =>
                setTimeout( -> 
                    self.state.switch('startSpin')
                , self.startDelay())
            @previous.state.observeOn('spinEnd').start 'Slot Ender', (from, to) =>
                setTimeout( -> 
                    self.state.switch('spinEnd')
                , self.endDelay())
        animationEnd: =>
            @state.switch("spinEnd")

        update: (newTiles) =>
            @tiles[i].update(newTiles[i]) for i in [0...@tiles.length]
