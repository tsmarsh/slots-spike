define ["lib/knockout-3.0.0", "js/slot", "js/skinny-coffee-machine", "lib/lodash"], (ko, Slot, SkinnyCoffeeMachine, jq) ->
    class Machine
        constructor: (@num_slots, @num_tiles, @states) ->
            @duration = 0.6
            @allowSpin = ko.observable(true)
            @events = 
                before:
                    startSpin: (from, to) =>
                        @spinAudio = new Audio('/static/audio/egypt_spin_s.mp3')
                on:
                    generateTiles: (from, to) =>
                        @generate()
                    calculateScores: (from, to) =>
                        @calculate()
                    ready: (from, to) =>
                        @allowSpin(true) 
                    startSpin: (from, to) =>
                        @allowSpin(false)
                        @spinAudio.play()
                        setTimeout => 
                            @slots[i].update(@game[i]) for i in [0...@num_slots]
                        , (@duration / 2) * 1000
                    spinEnd: (from, to) =>
                        @state.switch("reward")
                    reward: (from, to) =>
                        @reward()

            @state = new SkinnyCoffeeMachine jq.extend({}, @states, @events)

            slot_config = 
                num_tiles: @num_tiles
                startDelay: 0
                endDelay: @duration * 1000
                duration: @duration

            @slots = (new Slot(slot_config, @states) for i in [1..@num_slots])
            @slots[0].previous( @ )
            @slots[i].previous( @slots[i-1] ) for i in [1...@num_slots]
            @slots[@slots.length - 1].state.observeOn('spinEnd').start 'Slot Spinner', (from, to) =>
                @state.switch('reward')
            
            @state.switch('generateTiles')
            @slots[i].update(@game[i]) for i in [0...@num_slots]

        startSpin: =>
            @state.switch('startSpin')
            setTimeout =>
                @slots[0].state.switch('spinEnd')
            , @duration * 1000

        generate: =>
            rnd = =>
                Math.floor((Math.random()* 11)+1)

            padInt = (i) ->
                str = "" + i
                pad = "00"
                pad.substring(0, pad.length - str.length) + str

            @game = (( padInt(rnd()) for i in [0...@num_tiles]) for _ in [0...@num_slots])
            @state.switch('calculateScores')

        calculate: =>
            @state.switch('ready')

        reward: =>
            @state.switch('generateTiles')

