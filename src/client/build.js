({
    baseUrl: ".",
    name: "lib/almond",
    include: ["js/main"],
    includeRequire: ["js/main"],
    out: "../../static/js/slots.js",
    wrap: true
})